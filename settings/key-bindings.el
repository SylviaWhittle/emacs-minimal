;; KEY BINDINGS
;; ------------
;; List keybindings for (see https://emacs.stackexchange.com/q/732)...
;;
;; Buffer C-h b
;; Mode   C-h m
;;
;; Other options are...
;;
;; helm-descbinds
;; which-key
;; guide-key
;;
;; Most things commented out here reside under init.el > (use-package emacs :bind (...))
;; https://github.com/justbur/emacs-which-key
(use-package which-key
	     :config (which-key-mode))

;; helpful settings
;; https://github.com/Wilfred/helpful
;;
(use-package helpful
	     :config
	     ;; Note that the built-in `describe-function' includes both functions
	     ;; and macros. `helpful-function' is functions only, so we provide
	     ;; `helpful-callable' as a drop-in replacement.
	     (global-set-key (kbd "C-h f") #'helpful-callable)
	     (global-set-key (kbd "C-h v") #'helpful-variable)
	     (global-set-key (kbd "C-h k") #'helpful-key)
	     ;; Lookup the current symbol at point. C-c C-d is a common keybinding
	     ;; for this in lisp modes.
	     (global-set-key (kbd "C-c C-d") #'helpful-at-point)

	     ;; Look up *F*unctions (excludes macros).
	     ;;
	     ;; By default, C-h F is bound to `Info-goto-emacs-command-node'. Helpful
	     ;; already links to the manual, if a function is referenced there.
	     (global-set-key (kbd "C-h F") #'helpful-function)

	     ;; Look up *C*ommands.
	     ;;
	     ;; By default, C-h C is bound to describe `describe-coding-system'. I
	     ;; don't find this very useful, but it's frequently useful to only
	     ;; look at interactive functions.
	     (global-set-key (kbd "C-h C") #'helpful-command))

;; Globally set keys
(global-set-key (kbd "C-c C-r") 'revert-buffer-no-confirm)
(global-set-key (kbd "C-c p v") 'pyvenv-workon)
;; Function Keys
(global-set-key (kbd "<f1>") 'password-store-copy)
(global-set-key (kbd "<f2>") 'eval-buffer)
(global-set-key (kbd "<f3>") 'eval-region)
(global-set-key (kbd "<f4>") 'package-list-packages)
;; EIN commands
(local-set-key (kbd "<f10>") 'ein:notebook-reconnect-kernel)
(local-set-key (kbd "<f11>") 'ein:worksheet-delete-cell)
